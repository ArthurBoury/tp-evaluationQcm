package domain;

import exception.QuestionException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuestionTest {
    Answer[] responses = {
            new Answer("A", "Héritage", true),
            new Answer("B", "Polymorphisme", true),
            new Answer("C", "Encapsulation", true),
            new Answer("D", "Compilation", false)
    };

    Question question1 = new Question(1, "Lequel des éléments suivants n’est pas un concept POO en Java?", responses, " blabla");

    @Test
    void testEmptyQuestion() {
        QuestionException e = assertThrows(QuestionException.class, () -> new Question(1, "", responses, "blabla"));
    }

    @Test
    void testEmptyExplication(){
        QuestionException e = assertThrows(QuestionException.class, () -> new Question(1, "nblblzaz", responses, ""));
    }
    @Test
    void testCustomerChoicesAreGood() {
        String[] customerChoices = {"A", "B", "C"};
        assertTrue(question1.isValidate(customerChoices));
    }

    @Test
    void testCustomerChoicesAreBad() {
        String[] customerChoices = {"C", "B"};
        assertFalse(question1.isValidate(customerChoices));
    }

    @Test
    void testCustomerChoicesNotOrdered() {
        String[] customerChoices = {"B", "A", "C"};
        assertTrue(question1.isValidate(customerChoices));
    }

    @Test
    void testCustomerChoicesNotValid() {
        String[] customerChoices = {"dzaio", "1", "oiudzau"};
        assertFalse(question1.isValidate(customerChoices));
    }


}