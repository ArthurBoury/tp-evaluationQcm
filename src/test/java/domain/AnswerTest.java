package domain;

import exception.AnswerException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnswerTest {

    @Test
    void testEmptyAnswer() {
        AnswerException e = assertThrows(AnswerException.class, () -> new Answer("A", "", true));
    }

    @Test
    void testEmptyAnswerId() {
        AnswerException e = assertThrows(AnswerException.class, () -> new Answer("", "Java est un langage orienté objet", true));
    }

    @Test
    void testAnswerIdLength() {
        AnswerException e = assertThrows(AnswerException.class, () -> new Answer("java", "Java est un langage orienté objet", true));

    }
}