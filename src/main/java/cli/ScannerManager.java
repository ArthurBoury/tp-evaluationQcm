package cli;

import java.util.Scanner;

public class ScannerManager {
    private static final Scanner scanner = new Scanner(System.in);
    private static final ScannerManager INSTANCE = new ScannerManager();

    public static ScannerManager getInstance() {
        return INSTANCE;
    }

    private ScannerManager() {
    }

    public String[] getCustomerAnswer() {
        String[] answers = null;
        do {
            try {
                answers = scanner.nextLine().split(" ");
            } catch (Exception e) {
                System.out.println("Choix invalide !");
            }
        }
        while (answers == null);
        return answers;
    }


    public int getCustomerIntChoice() {
        Integer choice = null;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("Choix invalide !");
            }
        } while (choice == null);
        return choice;
    }

    public String getCustomerName() {
        return scanner.nextLine();
    }
}
