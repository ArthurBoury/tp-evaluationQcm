package cli.factory;

import domain.CustomerScore;

public class CustomerScoreFactory {
    public static CustomerScore createCustomerScore(String name, int score) {
        return new CustomerScore(name, score);
    }
}
