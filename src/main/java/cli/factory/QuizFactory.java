package cli.factory;

import domain.Answer;
import domain.Question;
import domain.Quiz;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class QuizFactory {

    public static Quiz createQuiz(String fileName) {
        String inputFile = "src/main/resources/" + fileName;
        Quiz quiz = new Quiz(fileName.substring(0, fileName.lastIndexOf('.')));
        try {
            List<String> lines = Files.readAllLines(Path.of(inputFile).toAbsolutePath());

            for (String line : lines.subList(1, lines.size())) {
                String[] question = line.split(";");
                String[] answersContent = question[2].split(",");
                Answer[] answers = new Answer[answersContent.length];
                String[] goodAnswers = question[4].split(",");

                for (int i = 0; i < answers.length; i++) {
                    for (int j = 0; j < goodAnswers.length; j++) {
                        if (goodAnswers[j].equals(getCharForNumber(i + 1))) {
                            answers[i] = new Answer(getCharForNumber(i + 1), answersContent[i], true);
                        } else {
                            answers[i] = new Answer(getCharForNumber(i + 1), answersContent[i], false);
                        }
                    }
                }
                quiz.addQuestion(new Question(Integer.parseInt(question[0]), question[1], answers, question[3]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return quiz;
    }

    private static String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char) (i + 'A' - 1)) : null;
    }
}
