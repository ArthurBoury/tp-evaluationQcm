package cli;

import cli.screen.MenuScreen;
import cli.screen.Screen;
import cli.factory.QuizFactory;

public class Main {
    public static void main(String[] args) {
        QuizManager.addQuiz(QuizFactory.createQuiz("Collection.csv"));
        QuizManager.addQuiz(QuizFactory.createQuiz("Programmation Orientee Objet.csv"));
        Screen.launchWorkflow(new MenuScreen());
    }
}
