package cli;

import domain.Quiz;

import java.util.ArrayList;
import java.util.List;

public class QuizManager {
    private static List<Quiz> quizList = new ArrayList<>();

    public static List<Quiz> getQuizList() {
        return quizList;
    }

    public static void addQuiz(Quiz quiz) {
        quizList.add(quiz);
    }

    public static Quiz getQuiz(int customerQcmChoice) {
        return quizList.get(customerQcmChoice);
    }
}
