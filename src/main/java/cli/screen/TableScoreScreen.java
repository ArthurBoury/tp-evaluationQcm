package cli.screen;

import domain.CustomerScore;
import domain.Quiz;

public class TableScoreScreen implements Screen {
    private Quiz quiz;

    public TableScoreScreen(Quiz quiz) {
        this.quiz = quiz;
    }

    @Override
    public Screen render() {
        System.out.println("= Tableau des scores du QCM Java - " + quiz.getName());
        int index = 1;
        for (CustomerScore customerScore : quiz.getScoreTable().getScoreTable()) {
            System.out.println(index++ + " - " + customerScore.getName() + " " + customerScore.getScore() + "/" + quiz.getQuiz().size() + " " + customerScore.getDate());
        }
        return new MenuScreen();
    }
}
