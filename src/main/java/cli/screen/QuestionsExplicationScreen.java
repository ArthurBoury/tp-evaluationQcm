package cli.screen;

import domain.Question;
import domain.Quiz;

import java.util.Map;

public class QuestionsExplicationScreen implements Screen {
    private Quiz quiz;

    public QuestionsExplicationScreen(Quiz quiz) {
        this.quiz = quiz;
    }

    @Override
    public Screen render() {
        System.out.println("\n= Réponses du QCM\n");
        for (Map.Entry<Question, String[]> entry : quiz.getCorrectionQuiz().entrySet()) {
            System.out.println(entry.getKey().toString());

            System.out.print("Votre réponse : ");
            for (String customerAnswer : entry.getValue()) {
                System.out.print(customerAnswer + " ");
            }
            System.out.print("\nReponse correcte : ");
            for (int i = 0; i < entry.getKey().getAnswers().length; i++) {
                if (entry.getKey().getAnswers()[i].isGoodAnswer()) {
                    System.out.print(entry.getKey().getAnswers()[i].getId() + " ");
                }
            }
            System.out.println();
        }
        return new MenuScreen();
    }
}
