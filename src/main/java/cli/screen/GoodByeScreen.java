package cli.screen;

public class GoodByeScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println("Bye bye...");
        return null;
    }
}
