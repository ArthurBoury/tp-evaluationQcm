package cli.screen;

import cli.ScannerManager;
import cli.QuizManager;

public class MenuScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println("""
                === QCM WayToLearnX ===
                                
                Quel QCM souhaitez vous réalisez ?
                  """);

        int i = 0; //Nombre ligne dans le menu

        for (i = 0; i < QuizManager.getQuizList().size(); i++) {
            System.out.println(i + " - Java - " + QuizManager.getQuizList().get(i).getName());
        }
        System.out.println(i + " - Tableau des scores");
        i++;
        System.out.println(i + " - Quitter l'application\n");
        System.out.println("Votre choix: <saisie de l’utilisateur >");

        int customerQcmChoice = ScannerManager.getInstance().getCustomerIntChoice();

        if (customerQcmChoice < QuizManager.getQuizList().size() && customerQcmChoice >= 0) {
            return new QuizScreen(QuizManager.getQuiz(customerQcmChoice));
        } else if (customerQcmChoice == i - 1) {
            return new TableScoreMenuScreen();
        } else if (customerQcmChoice == i) {
            return new GoodByeScreen();
        } else {
            return this;
        }
    }
}

