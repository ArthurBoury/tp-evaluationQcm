package cli.screen;

import cli.QuizManager;
import cli.ScannerManager;
import domain.CustomerScore;
import domain.Quiz;

public class TableScoreMenuScreen implements Screen {
    @Override
    public Screen render() {
        System.out.println("""                               
                = Tableaux des scores
                                
                Quel QCM souhaitez-vous consulter le tableau des scores ?
                """);

        for (int i = 0; i < QuizManager.getQuizList().size(); i++) {
            System.out.println(i + " - Java - " + QuizManager.getQuizList().get(i).getName());
        }
        Quiz quiz = null;
        do {
            int customerChoice = ScannerManager.getInstance().getCustomerIntChoice();
            try {

                quiz = QuizManager.getQuizList().get(customerChoice);

            } catch (IndexOutOfBoundsException e) {
                System.out.println("Le quizz n'existe pas !");
            }
        } while (quiz == null);

        return new TableScoreScreen(quiz);
    }
}
