package cli.screen;

public interface Screen {
    static void launchWorkflow(Screen firstScreen) {
        Screen screenToDisplay = firstScreen;
        while (screenToDisplay != null) {
            screenToDisplay = screenToDisplay.render();

            if (screenToDisplay != null) {
                System.out.println("--------------------------");
            }
        }
    }

    Screen render();
}
