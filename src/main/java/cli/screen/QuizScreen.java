package cli.screen;

import cli.ScannerManager;
import domain.ScoreTable;
import cli.factory.CustomerScoreFactory;
import domain.Question;
import domain.Quiz;

public class QuizScreen implements Screen {
    private Quiz quiz;

    public QuizScreen(Quiz quiz) {
        this.quiz = quiz;
    }

    @Override
    public Screen render() {
        int i = 0;
        int score = 0;
        do {
            System.out.println("=== QCM Java -" + quiz.getName() + "\n");
            Question question = quiz.getQuiz().get(i);
            System.out.println(question.toString());
            String[] customerAnswer = ScannerManager.getInstance().getCustomerAnswer();

            if (question.isValidate(customerAnswer)) {
                score++;
            } else {
                quiz.addQuestionCorrection(question, customerAnswer);
            }
            i++;
        } while (i < quiz.getQuiz().size());
        System.out.println("= Résultat du QCM");
        System.out.println("Votre score est de " + score + "/" + quiz.getQuiz().size());

        if (score == quiz.getQuiz().size()) {
            System.out.println("Bravo ! Vous avez fait un sans faute et obtenu un 10/10.\n");
            return new MenuScreen();
        }

        //AJOUT AU TABLEAU DES SCORE
        System.out.println("Quel est votre nom ? <saisie de l’utilisateur>\n");
        String name = ScannerManager.getInstance().getCustomerName();
        quiz.getScoreTable().addCustomerScore(CustomerScoreFactory.createCustomerScore(name, score));

        System.out.println("""
                Voulez-vous voir les réponses des questions vous avez échouées 
                (Y/N) ? <saisie de l’utilisateur>                                
                """);
        return switch (ScannerManager.getInstance().getCustomerAnswer()[0]) {
            case "Y" -> new QuestionsExplicationScreen(this.quiz);
            case "N" -> new MenuScreen();
            default -> new MenuScreen();
        };
    }
}
