package domain;

import cli.factory.CustomerScoreFactory;
import domain.CustomerScore;

import java.util.ArrayList;
import java.util.List;

public class ScoreTable {
    private List<CustomerScore> scoreTable = new ArrayList<>();

    public void addCustomerScore(CustomerScore customerScore) {
        scoreTable.add(customerScore);
        scoreTable.stream().mapToInt(CustomerScore::getScore).sorted();
    }

    public List<CustomerScore> getScoreTable() {
        return scoreTable;
    }
}
