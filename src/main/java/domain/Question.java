package domain;

import exception.QuestionException;

import java.util.Arrays;

public class Question {
    private int id;
    private String question;
    private Answer[] answers;
    private String answerExplication;

    public Question(int id, String question, Answer[] answers, String answerExplication) {
        if (question == null || question.equals("")) {
            throw new QuestionException("La question ne peut être vide !");
        }
        if (answerExplication == null || answerExplication.equals("")) {
            throw new QuestionException("L'explication ne peut être vide !");
        }
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.answerExplication = answerExplication;
    }

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public Answer[] getAnswers() {
        return this.answers;
    }

    public String toString() {
        StringBuilder questionBloc = new StringBuilder();
        questionBloc.append("= Question ").append(id).append("\n")
                .append(question).append("\n\n");
        for (Answer answer : answers) {
            questionBloc.append(answer.getId()).append(" - ").append(answer.getAnswer()).append("\n");
        }

        return questionBloc.toString();
    }

    private String getGoodAnswers() {
        StringBuilder goodAnswers = new StringBuilder();
        for (Answer answer : answers) {
            if (answer.isGoodAnswer()) {
                goodAnswers.append(answer.getId()).append(" ");
            }
        }
        return goodAnswers.toString();
    }

    public boolean isValidate(String[] customerChoices) {
        boolean[] customerBooleanChoices = new boolean[answers.length];
        boolean isValidate = false;
        for (String choice : customerChoices) {
            for (int i = 0; i < answers.length; i++) {
                if (choice.toUpperCase().equals(answers[i].getId())) {
                    customerBooleanChoices[i] = true;
                    break;
                }
            }
        }
        for (int i = 0; i < answers.length; i++) {
            isValidate = answers[i].isGoodAnswer() == customerBooleanChoices[i];
            if (!isValidate) {
                break;
            }
        }
        return isValidate;
    }
}
