package domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Quiz {
    private String name;

    private List<Question> quiz = new ArrayList<>();

    private Map<Question, String[]> correctionQuiz = new LinkedHashMap<>();

    private ScoreTable scoreTable;

    public Quiz(String name) {
        this.scoreTable = new ScoreTable();
        this.name = name;
    }

    public Quiz(String name, List<Question> quiz) {
        this.name = name;
        this.quiz = quiz;
    }

    public ScoreTable getScoreTable() {
        return scoreTable;
    }

    public void addQuestion(Question question) {
        this.quiz.add(question);
    }

    public void addQuestionCorrection(Question question, String[] customerAnswer) {
        this.correctionQuiz.put(question, customerAnswer);
    }

    public String getName() {
        return name;
    }

    public List<Question> getQuiz() {
        return quiz;
    }

    public Map<Question, String[]> getCorrectionQuiz() {
        return correctionQuiz;
    }
}
