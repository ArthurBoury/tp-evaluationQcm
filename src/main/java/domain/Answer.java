package domain;

import exception.AnswerException;

public class Answer {
    private String id;
    private String answer;
    private boolean isGoodAnswer;

    public boolean isGoodAnswer() {
        return isGoodAnswer;
    }

    public Answer(String id, String answer, boolean isGoodAnswer) {
        if (answer == null || answer.equals("")) {
            throw new AnswerException("La réponse est vide !");
        }
        if (id == null || id.equals("")) {
            throw new AnswerException("L'id ne peut être vide !");
        }
        if (id.length() > 1) {
            throw new AnswerException("L'id doit être un caractère !");
        }
        this.id = id;
        this.answer = answer;
        this.isGoodAnswer = isGoodAnswer;
    }

    public String getId() {
        return id;
    }


    public String getAnswer() {
        return answer;
    }

}
