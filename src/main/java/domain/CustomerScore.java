package domain;

import java.time.LocalDate;

public class CustomerScore {
    private int score;
    private String name;
    private LocalDate date;

    public CustomerScore(String name, int score) {
        this.score = score;
        this.name = name;
        this.date = LocalDate.now();
    }

    public LocalDate getDate() {
        return date;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }
}
