package exception;

import domain.Question;

public class QuestionException extends RuntimeException {

    public QuestionException() {
        super();
    }

    public QuestionException(String s) {
        super(s);
    }
}
