package exception;

public class AnswerException extends RuntimeException {
    public AnswerException() {
        super();
    }

    public AnswerException(String s) {
        super(s);
    }
}
